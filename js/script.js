function initMap() {
    var mapOptions = {
        center: new google.maps.LatLng(51.5, -0.12),
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.terrain
    };
    var map = new google.maps.Map(document.getElementById("map"), mapOptions);
}
if($(window).width()>912)
{
  $('.brand').css({'position':'fixed','top':'80px'});  
}
else
{
  $('.brand').css({'position':'fixed','top':'7px'});   
}

$(window).scroll(function(){
	var scrollTop=$(window).scrollTop();
	if (scrollTop>=673 && $(window).width()>768) {
		$('.menuHeader').css({'position':'fixed','top':'35px'});
    $('.brand').css({'position':'static','top':'718px'});
	}
	else{
		$('.menuHeader').css('position','static');
    $('.brand').css({'position':'fixed','top':'80px'});
	}
	console.log(scrollTop);
});

$(window).resize(function() {
  if ($(window).width() < 992) {
    jQuery("nav").addClass("navbar-fixed-top") ;
  }
 else {
   jQuery("nav").removeClass("navbar-fixed-top") ; 
 }
});
$(document).ready(function(){
    if ($(window).width() < 992) {
        console.log($(window).width());
        jQuery("nav").addClass("navbar-fixed-top") ;
    }
    else {
        jQuery("nav").removeClass("navbar-fixed-top") ; 
    }
  $('nav li a').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
    && location.hostname == this.hostname) {
      var $target = $(this.hash);
      $target = $target.length && $target
      || $('[name=' + this.hash.slice(1) +']');
      if ($target.length) {
        var targetOffset = $target.offset().top;
        $('html,body')
        .animate({scrollTop: targetOffset}, 1000);
       return false;
      }
    }
  });
});